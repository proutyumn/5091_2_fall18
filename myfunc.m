%[A,B]=myfunc(input_value)
%Where A = input_value
%Where B = input_value/2
function [A,B]=myfunc(input_value)
    %here we calculate the value of A and B
    A=supportingfn(input_value^2); 
    B=input_value/2;
    
end

function [G]=supportingfn(M)
    %this function takes the square root of a value
    G=sqrt(M);

end