function a=fiveminanswer(n,a)

    if(nargin<2)
        a=[];
    end
    if(length(a)<n)
        a(end+1)=rand;
        a=fiveminanswer(n,a);
    end

end
    